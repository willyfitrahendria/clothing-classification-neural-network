# Clothing Classification Neural Network

Classification of clothing using neural network on Fashion-MNIST dataset

Python 3.7.3

TensorFlow 2.0.0-beta1


Sample of prediction result
---

![](./output/prediction_result.png)

Accuracy
---

![](./output/accuracy.png)